# Pi Web Deck Frontend
This project aims at providing an alternative to the Stream Deck. 
The Decks content can be edited in an Editor and are Displayed on a touch screen connected to a Raspberry Pi.

Buttons can execute bash or shell commands, open programs or websites and (maybe) much more. See below for a full list 
of implemented Actions.

> ⚠ Currently, the backend server is not secured by a password or similar. As such i cannot recommend running this
> Software on **Any** device directly conncted to the internet (without a nat and or firewall) or public networks.
> **Use on your own risk**

![Project Overview](./readme/Overview.png)

## Hardware
This project was created to work with a Raspberry Pi 3 and 
a [7inch Capacitive Touch Screen LCD (H) with Case](https://www.waveshare.com/product/raspberry-pi/displays/lcd-oled/7inch-hdmi-lcd-h-with-case.htm)

STL and STEP files for a Case can be found in the Hardware folder. To use the case, additional 
(short) HDMI and USB to µUSB cables and (depending on the cables) 270°  HDMI and µUSB adapters are required.

(Please do check the correct orientation of the adapters before buying)

## Button Types
In this section the currently implemented button types are discussed. 
These are currently **only tested on Windows** but (hopefully) should work on Linux to.

### Action
Actions allow buttons to do something on the host machine.

#### Start Program
When the button is clicked, the Program specified will be executed.
Currently, no options can be defined.

![Start Program](./readme/StartProgram.png)

#### Batch Command
When the button is clicked, the Command specified will be executed by the backend. Currently, only single line commands 
are accepted (but multiple commands can be executed by combining them with &) 
(Shell commands should work on Linux systems, but these are currently not tested)

![Execute Command](./readme/ExecuteCommand.png)

#### (Not jet Implemented)Open Webpage
The specified webpage is opened.

### Status
Status Buttons allow you to display your systems Stats on a Button.

#### Sensors
Sensors represent the data that can be visualised on the buttons. Currently, following sensors are supported:

- CPU(s) percentage
- Memory percentage
- (Not jet visualised) Disk space percentage
- (Not jet visualised) Uptime

#### Visualisations
Visualisations take the data and make tem pretty. Not all Visualisations are Available for all Sensors.

- Line: [Nivo](https://nivo.rocks/) Line Graphs to visualize Data over Time
    - Available for: CPU(s) and Memory
- Gauge: [react-liquid-gauge](https://github.com/trendmicro-frontend/react-liquid-gauge) visualisation
    - Available for: CPU(s), Memory and Disk(s)
- (Not implemented jet) Single Stat:
    - Available for: CPU(s), Memory, Disk(s) and Uptime
    
#### Average
Some sensors can be averaged to display a single stat instead of multiple. This can be especially 
helpful for CPUs and Disk as to many visualisations ca become confusing.
Currently, the following Sensors can be Averaged:
- CPUs
- Disks

### Link
When clicked, the currently visible Panel will be switched.

![Link](./readme/Link.png)

### Reload Panel
When clicked, the website will reload. This is especially usefull to reload the Panels and Buttons.


### MQTT
When clicked, a static message is transmitted to the specified topic. This feature is only visible, if the Server  
has the MQTT feature enabled and could establish a connection to the MQTT Server. Otherwise, the MQTT type button will 
not be visible.

> Currently, only one Server is supported which can be set in the server configuration. Information on how 
> to configure the Server can be found at [pi-web-deck-backend](https://gitlab.com/Dentur/pi-web-deck-backend)

## Graphical Editor
As many images already shown a graphical editor is visible on the right side of the Panel. This Editor is only visible
on bigger Monitors and not on the Web Deck itself.

In the Editor new Panels and Buttons can be added by clicking the corresponding Buttons in the second
Sidebar (form the right).
The Panel itself can be edited by selecting "Start Editing" on the rightmost Sidebar. In the editing mode, the Buttons 
in the Panel can be Moved via Drag & Drop and clicked to select them. When a Button is selected, its properties can be 
changed in the right most Sidebar.


## Installation

### Requirements
- Rustlang (1.47.0-nightly or higher) to build the backend
- yarn to compile the frontend on start of the backend
- (somewhat) windows 10. While the Project was created without a specific OS, currently i only need and thus test on 
  windows. If it can be executed on Linux (or other Platforms) please let me know!

Currently, the best way to install this project is by building it from Source. To do so, the
[pi-web-deck-backend](https://gitlab.com/Dentur/pi-web-deck-backend) needs to be 
cloned **with the `--recurse-submodules`** command, as the Backend will also host the frontend component.

`git clone https://gitlab.com/Dentur/pi-web-deck-backend.git --recurse-submodules`

After the backend was cloned, it can be build with 

`cargo build --release`

Once the build finishes the program can be started by executing `./target/release/stream-deck-pi-backend`. It should 
be put in autostart by your preferred method.


