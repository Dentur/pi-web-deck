/**
 * Created by Sebastian Venhuis on 15.11.2020.
 */
import {addImageAsync, imagesSelector} from "../../state/ImageState";
import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import {Popup} from "../Utils/Popup/Popup";
import "../Utils/inputs.css"
import {useDropzone} from "react-dropzone";
import {faImages} from "@fortawesome/free-regular-svg-icons";
import {library} from "@fortawesome/fontawesome-svg-core";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import "./ImageSelector.css"
import {v4} from "uuid";
import PropTypes from "prop-types"

library.add(faImages);



const thumbsContainer = {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 16
};

const thumb = {
    display: 'inline-flex',
    borderRadius: 2,
    border: '1px solid #eaeaea',
    marginBottom: 8,
    marginRight: 8,
    width: 100,
    height: 100,
    padding: 4,
    boxSizing: 'border-box'
};

const thumbInner = {
    display: 'flex',
    minWidth: 0,
    overflow: 'hidden'
};

const img = {
    display: 'block',
    width: 'auto',
    height: '100%'
};

const ImageUpload = function(props){
    const [files, setFiles] = useState([]);
    const {
        getRootProps,
        getInputProps,
        isDragActive,
        isDragAccept,
        isDragReject
    } = useDropzone({
        onDrop: (images)=>{
            setFiles(images.map(file => Object.assign(file, {
                preview: URL.createObjectURL(file)
            })));
        },
        accept: "image/*"
    });

    const thumbs = files.map(file => (
        <div style={thumb} key={file.name}>
            <div style={thumbInner}>
                <img
                    src={file.preview}
                    style={img}
                />
            </div>
        </div>
    ));

    useEffect(() => () => {
        // Make sure to revoke the data uris to avoid memory leaks
        files.forEach(file => URL.revokeObjectURL(file.preview));
    }, [files]);

    return (
        <section className="ImageUploadContainer">
            <div {...getRootProps({className: 'dropzone'})}>
                <input {...getInputProps()} />
                <div className={"ImageDropArea"}>
                    <FontAwesomeIcon style={{fontSize: "larger"}}  icon={faImages}/>
                    {isDragAccept && (<span>All files will be accepted</span>)}
                    {isDragReject && (<span>Some files will be rejected</span>)}
                    {!isDragActive && (<span>Drop Images here ...</span>)}
                </div>
            </div>
            {files.length > 0 ?
            <aside style={thumbsContainer}>
                <ul>{thumbs}</ul>
            </aside> : null }
            {files.length > 0 ?
                <div className={"ImageSelectorUploadActions"}>
                    <button className={"sdpButton"} onClick={() => {
                        props.onAccept(files);
                        setFiles([]);
                    }}>
                        Upload
                    </button>
                    <button className={"sdpButton abort"} onClick={() => {
                        setFiles([])
                    }}>Abort
                    </button>
                </div>
            : null}
        </section>
    );
}

class ImageSelectorClass extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            selected: props.selected
        }
    }

    async onAccept(pictureFiles){
        //console.log("onDrop", pictureFiles)


        let pictureDataUis = await Promise.all(pictureFiles.map( (file)=>{
            return new Promise((resolve, reject)=>{
                const reader = new FileReader();
                reader.addEventListener("load", ()=>{
                    let url = reader.result;
                    resolve({
                        id: v4(),
                        name: file.name.split(".")[0],
                        data: url
                    });
                })
                reader.readAsDataURL(file);
            })
        }))

        pictureDataUis.forEach((image)=>{
            //console.log(image);
            this.props.addImageAsync(image)
        })


        //this.props.addImageAsync()
    }

    render(){

        return <div className={"ImageSelector"}>
            <div className={"content"}>
                <div className={"existingImages"}>
                    <div className={"ImageWrapper " + ((!this.state.selected) ? "selected": "")} onClick={()=>this.setState({selected: null})}>
                        <div><img/></div><span>None</span>
                    </div>
                    {this.props.images.map((image)=>{
                        //console.log(image);
                        let isSelected = (image.id === this.state.selected) ? "selected" : "";
                        return <div key={image.id} className={"ImageWrapper " + isSelected} onClick={()=>{
                            this.setState({selected: image.id})
                        }}>
                            <div>
                                <img src={image.data}/>
                            </div>
                            <span>{image.name}</span>
                        </div>
                    })}
                </div>
                <div className={"newImage"}>
                    <ImageUpload onAccept={(files)=>this.onAccept(files)}></ImageUpload>
                </div>
            </div>
            <footer>
                <div className={"sdpButton"} onClick={()=>{
                    if(this.props.onSelected){
                        this.props.onSelected(this.state.selected);
                    }
                }}>Save</div>
            </footer>
        </div>
    }
}


const mapStateToProps = (state)=>{
    return{
        images: imagesSelector(state)
    }
}

const mapActionsToProps = {
    addImageAsync
}

export const ImageSelector = connect(mapStateToProps, mapActionsToProps)(ImageSelectorClass)


ImageSelector.propTypes = {
    selected: PropTypes.string,
    onClose: PropTypes.func,
    onSelected: PropTypes.func
}
