/**
 * Created by Sebastian Venhuis on 22.12.2020.
 */

import React from "react"
import {connect} from "react-redux";
import "./ButtonSidebar.css"
import {EditingField} from "./EditingField/EditingField";
import {ButtonProperties} from "../ButtonProperties/ButtonProperties";
import {
    addButtonAsync, addDefaultButtonAsync, addDefaultPanelAsync,
    addPanelAsync, currentPanelSelector,
    panelsLoadedSelector,
    panelsSelector,
    removeButtonAsync,
    removePanelAsync, updatePanel, updatePanelAsync
} from "../../state/PanelState";
import "../Utils/button.css"
import {currentlyEditedButtonIdSelector, currentPanelIdSelector, showPanel} from "../../state/ViewState";

class PanelSidebarClass extends React.Component {
    render(){
        return <div className={"Sidebar"} style={{width: 300}}>
            <div className={"content"}>
                <select className={"spdSelect"} value={this.props.selectedPanelId} onChange={(e)=>{
                    this.props.showPanel(e.target.value);
                }}>
                    {this.props.panels.map((panel)=>{
                        return <option key={panel.id} value={panel.id}>
                            {panel.name}
                        </option>
                    })}
                </select>
                <div className={"buttonContainer"}>
                    <div className={"button"} onClick={()=>{
                        this.props.addDefaultPanelAsync()
                    }}>
                        Add Panel
                    </div>
                    <div className={"button"} onClick={()=>{
                        this.props.addDefaultButtonAsync(this.props.selectedPanelId)
                    }}>
                        Add Button
                    </div>
                </div>
                <div className={"buttonContainer"}>
                    <div className={"button"} onClick={()=>{
                        this.props.removePanelAsync(this.props.selectedPanelId);
                    }}>
                        Remove Selected Panel
                    </div>
                    {this.props.selectedButton ? <div className={"button"} onClick={()=>{
                        this.props.removeButtonAsync(this.props.selectedPanelId, this.props.selectedButton);
                    }}>
                        Remove Selected Button
                    </div> : <div className={"button disabled"}>Remove Selected Button</div>}
                </div>
                <div className={"buttonContainer"}>
                    <span>Panel Name</span>
                    <input type={"text"} value={this.props.selectedPanel.name} onChange={(e)=>{
                        this.props.updatePanelAsync(this.props.selectedPanelId, {
                            ...this.props.selectedPanel,
                            name: e.target.value
                        })
                    }}/>
                </div>
            </div>
        </div>
    }
}

const mapStateToProps = (state)=>{
    return {
        loaded: panelsLoadedSelector(state),
        panels: panelsSelector(state),
        selectedPanelId: currentPanelIdSelector(state),
        selectedPanel: currentPanelSelector(state),
        selectedButton: currentlyEditedButtonIdSelector(state)
    }
}

const mapActionsToState = {
    removeButtonAsync,
    removePanelAsync,
    showPanel,
    addDefaultPanelAsync,
    addDefaultButtonAsync,
    updatePanelAsync

}

export const PanelSidebar = connect(mapStateToProps, mapActionsToState)(PanelSidebarClass);
