/**
 * Created by Sebastian Venhuis on 08.11.2020.
 */

import React from "react"
import {connect} from "react-redux";
import "./ButtonSidebar.css"
import {EditingField} from "./EditingField/EditingField";
import {ButtonProperties} from "../ButtonProperties/ButtonProperties";
import {panelsLoadedSelector, panelsSelector} from "../../state/PanelState";
import "../Utils/button.css"

class ButtonSidebarClass extends React.Component {
    render(){
        return <div className={"Sidebar"} style={{alignSelf: "flex-end"}}>
            <div className={"content"}>
                <EditingField/>
                {this.props.loaded? <ButtonProperties/> : null }
            </div>
        </div>
    }
}

const mapStateToProps = (state)=>{
    return {
        loaded: panelsLoadedSelector(state),
        panels: panelsSelector(state)
    }
}

const mapActionsToState = {

}

export const ButtonSidebar = connect(mapStateToProps, mapActionsToState)(ButtonSidebarClass);
