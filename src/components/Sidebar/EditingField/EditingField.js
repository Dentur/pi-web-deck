/**
 * Created by Sebastian Venhuis on 08.11.2020.
 */

import React from "react";
import {connect} from "react-redux";
import {editingModeEnabledSelector, setEditing} from "../../../state/ViewState";
import "./EditingField.css"

class EditingFieldClass extends React.Component{
    render(){
        return <div className={"EditingField"}>
            {this.props.editing ? <div className={"editingButton editingMode"} onClick={()=>{this.props.setEditing(false)}}>Stop Editing</div> :
                <div className={"editingButton notEditing"} onClick={()=>{this.props.setEditing(true)}}>Start Editing</div>}
        </div>
    }
}

const mapStateToProps = (state)=>{
    return {
        editing: editingModeEnabledSelector(state)
    }
}

const mapActionToState = {
    setEditing
}

export const EditingField = connect(mapStateToProps, mapActionToState)(EditingFieldClass)

