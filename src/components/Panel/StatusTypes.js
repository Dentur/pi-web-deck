/**
 * Created by Sebastian Venhuis on 01.11.2020.
 */

export const StatusTypes = {
    CPU: "CPU",
    UPTIME: "UPTIME",
    DISKS: "DISK",
    MEMORY: "MEMORY"
}
