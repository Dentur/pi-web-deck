/**
 * Created by Sebastian Venhuis on 01.11.2020.
 */

import React from "react";
import "./GridPanel.css"
import ReactGridLayout from "react-grid-layout";
import PropTypes from "prop-types";
import {PanelButton} from "../Button/PanelButton";
import {v4 as uuid} from "uuid"
import {currentPanelButtonsSelector, currentPanelSelector, updateButtonAsync} from "../../state/PanelState";
import {connect} from "react-redux";
import {currentPanelIdSelector, editingModeEnabledSelector} from "../../state/ViewState";


class GridPanelClass extends  React.Component {
    constructor(props) {
        super(props)
    }

    getLayoutData(button){
        console.log("Layout", button.layoutOptions)
        return {
            x: button.layoutOptions.positionX,
            y: button.layoutOptions.positionY,
            w: button.layoutOptions.width,
            h: button.layoutOptions.height
        }
    }

    getLayout(){
        return this.props.buttons.map((button)=>{
            return {
                ...this.getLayoutData(button),
                i: button.id
            }
        })
    }

    render(){
        return <div className={"GridPanel"}>
            <ReactGridLayout className={"GridPanelLayout"}
                             layout={this.getLayout()}
                             cols={16}
                             rowHeight={55}
                             width={1004}
                             autoSize={false}
                             onLayoutChange={(layouts)=>{
                                 console.info("Layout changed", layouts);
                                 layouts.forEach((layout)=>{
                                     let button = this.props.buttons.find((panel)=>panel.id === layout.i);
                                     if(!button)
                                         return;
                                     if(button.layoutOptions.positionX !== layout.x || button.layoutOptions.positionY !== layout.y) {
                                         this.props.updateButtonAsync(this.props.selectedPanelId, button.id, {
                                             ...button,
                                             layoutOptions: {
                                                 ...button.layoutOptions,
                                                 positionX: layout.x,
                                                 positionY: layout.y
                                             }
                                         })
                                     }
                                 })
                             }}
                             preventCollision={true}
                             verticalCompact={false}
                             isBounded={true}
                             isDraggable={this.props.editing}
                             isResizable={true}
            >
                {this.props.buttons.map((button)=>{
                    //return <div key={button.id} data-grid={this.getLayoutData(button)}><PanelButton buttonId={button.id}/></div>
                    return <div key={button.id}><PanelButton buttonId={button.id}/></div>
                })}
            </ReactGridLayout>
        </div>
    }
}

GridPanelClass.propTypes = {
    buttons: PropTypes.array.isRequired
}



const mapStateToProps = (state)=>{
    return {
        selectedPanelId: currentPanelIdSelector(state),
        buttons: currentPanelButtonsSelector(state),
        editing: editingModeEnabledSelector(state)
    }
}

const mapActionsToProps = {
    updateButtonAsync
}

export const GridPanel = connect(mapStateToProps, mapActionsToProps)(GridPanelClass)
