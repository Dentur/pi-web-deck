/**
 * Created by Sebastian Venhuis on 01.11.2020.
 */

export const ActionTypes = {
    START_PROGRAM: "START_PROGRAM",
    OPEN_WEB_PAGE: "OPEN_WEB_PAGE",
    COMMAND: "COMMAND"
}
