/**
 * Created by Sebastian Venhuis on 27.12.2020.
 */

export const StatusDisplayTypes = {
    GAUGE: "GAUGE",
    LINE: "LINE",
    SINGLE: "SINGLE"
}
