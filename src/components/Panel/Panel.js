/**
 * Created by Sebastian Venhuis on 01.11.2020.
 */

import React from "react";
import PropTypes from "prop-types"
import {PanelLayoutOptions} from "./PanelLayoutOptions";
import {GridPanel} from "./GridPanel";
import "./Panel.css"
import {connect} from "react-redux";
import {currentPanelSelector} from "../../state/PanelState";
import Clock from 'react-live-clock';

class PanelClass extends React.Component{
    constructor(props) {
        super(props);
    }

    render(){
        return <div className={"panel"}>
            <header>{this.props.panel.name}</header>
            <div className={"content"}>
                {this.props.panel.layout === PanelLayoutOptions.GRID ? <GridPanel buttons={this.props.panel.buttons}/> : null}
            </div>
            <footer><Clock format={"DD.MM.YY HH:mm"} ticking={true}/></footer>
        </div>
    }
}

PanelClass.propTypes = {
    panel: PropTypes.shape({
        layout: PropTypes.string.isRequired,
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        buttons: PropTypes.array
    }).isRequired,
}

const mapStateToProps = (state)=>{
    return {
        panel: currentPanelSelector(state)
    }
}

const mapActionsToProps = {

}

export const Panel = connect(mapStateToProps, mapActionsToProps)(PanelClass)
