/**
 * Created by Sebastian Venhuis on 01.11.2020.
 */

export const ButtonTypes = {
    LINK: "LINK",
    ACTION: "ACTION",
    STATUS: "STATUS",
    RELOAD: "RELOAD_WEBSITE",
    MQTT: "MQTT",
    MATH: "MATH"
}
