/**
 * Created by Sebastian Venhuis on 31.10.2020.
 */

import React from "react";
import "./PanelController.css";
import {PanelLayoutOptions} from "./PanelLayoutOptions";
import {ButtonTypes} from "./ButtonTypes";
import {ActionTypes} from "./ActionTypes";
import {Panel} from "./Panel";
import {connect} from "react-redux";
import {
    currentPanelButtonSelector,
    fetchPanelsAsync,
    panelsLoadedSelector,
    panelsLoadingSelector
} from "../../state/PanelState";
import {fetchImagesAsync, imagesLoadedSelector, imagesLoadingSelector} from "../../state/ImageState";
import {startStatsPollAsync, statsPollRunningSelector} from "../../state/StatsState";

const sleep = m => new Promise(r => setTimeout(r, m))

class PanelControllerClass extends React.Component{

    constructor(props){
        super(props);
    }

    render(){
        return <div className={"PanelController"}>
            {!this.props.loaded ? <div>
                    {!this.props.panelsLoaded ? <span>Loading Panels</span> : null}
                    {!this.props.imagesLoaded ? <span>Loading Images</span> : null}
                </div> :
                <Panel/>
            }
        </div>
    }
}

const mapStateToProps = (state)=>{
    return {
        panelsLoaded: panelsLoadedSelector(state),
        imagesLoaded: imagesLoadedSelector(state),
        loaded: panelsLoadedSelector(state) && imagesLoadedSelector(state),
        statsPolling: statsPollRunningSelector(state),
    }
}

const mapActionsToProps = {
    fetchImagesAsync,
    fetchPanelsAsync,
    startStatsPollAsync,
}

export const PanelController = connect(mapStateToProps, mapActionsToProps)(PanelControllerClass)
