/**
 * Created by Sebastian Venhuis on 27.01.2021.
 */

export const MqttActionTypes = {
    NONE: "NONE",
    SEND: "SEND"
}
