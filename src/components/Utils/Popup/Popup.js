/**
 * Created by Sebastian Venhuis on 18.11.2020.
 */

import PropTypes from "prop-types"
import React from "react";
import {library} from "@fortawesome/fontawesome-svg-core";
import {faTimes} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import "./Popup.css"

library.add(faTimes)

export class Popup extends React.Component{
    render(){
        return <div className={"PopupWrapper"}>
            <div className={"Popup"}>
                <header><span>{this.props.title}</span><div className={"PopupClose"} onClick={()=>{
                    if(this.props.onClose)
                        this.props.onClose()
                }}><FontAwesomeIcon icon={faTimes}/></div></header>
                <div className={"PopupContent"}>
                    {this.props.children}
                </div>
            </div>
        </div>
    }
}

Popup.propTypes = {
    title: PropTypes.string,
    onClose: PropTypes.func
}
