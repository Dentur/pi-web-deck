/**
 * Created by Sebastian Venhuis on 15.11.2020.
 */

import React from "react";
import PropTypes from "prop-types"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faImage} from "@fortawesome/free-regular-svg-icons";
import {library} from "@fortawesome/fontawesome-svg-core";
import "./ButtonImage.css"
import {connect} from "react-redux";
import {imageSelector} from "../../../state/ImageState";

library.add(faImage);

export class ButtonImageClass extends React.Component{
    render(){
        return <div style={{...this.props.style}} className={"ButtonImage"} onClick={()=>{
            if(this.props.onClick)
                this.props.onClick()
        }}>
            {this.props.imageData?.data ? <img src={this.props.imageData.data}/> : <FontAwesomeIcon icon={faImage}/> }
        </div>
    }
}

const mapStateToProps = (state, props)=>{
    return {
        imageData: imageSelector(state, props.image)
    }
}

export const ButtonImage = connect(mapStateToProps)(ButtonImageClass)

ButtonImage.propTypes = {
    image: PropTypes.string,
    onClick: PropTypes.func,
    style: PropTypes.any
}
