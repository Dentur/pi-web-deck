/**
 * Created by Sebastian Venhuis on 12.11.2020.
 */


import React from "react";
import PropTypes from "prop-types"
import "./ButtonGroup.css"

export class ButtonGroup extends React.Component{

    constructor(props) {
        super(props);
    }

    render(){
        return <div className={"ButtonGroup"}>
            {this.props.children.map((element)=>{
               // console.log(element);
                //if(element.type !== ButtonGroupButton)
                //    throw new Error("ButtonGroup has child of non ButtonGroupButton");
                let className = "ButtonGroupButtonWrapper";
                if(element.props.value === this.props.value)
                    className += " Selected"
                return <div className={className} key={element.props.value} onClick={()=>this.props.onSelect(element.props.value)}>
                    {element}
                </div>
            })}
        </div>
    }
}



export class ButtonGroupButton extends React.Component{
    render(){
        return <div className={"ButtonGroupButton"}>{this.props.children}</div>
    }
}

ButtonGroupButton.propTypes = {
    value: PropTypes.string.isRequired,
    children: PropTypes.any,
}



ButtonGroup.propTypes = {
    onSelect: PropTypes.func,
    value: PropTypes.any.isRequired
}

