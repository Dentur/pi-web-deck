/**
 * Created by Sebastian Venhuis on 27.01.2021.
 */
import * as React from "react";
import {connect} from "react-redux";
import {executeInitialLoad, initialLoadingFinishedSelector} from "../../../state/InitialLoadingState";

class InitialLoadingClass extends React.Component{
    componentDidMount() {
        if(!this.props.initialLoadingFinished)
        {
            this.props.executeInitialLoad()
        }
    }

    render(){
        if(this.props.initialLoadingFinished)
            return this.props.children;
        else{
            return <span>Loading</span>
        }
    }
}

const mapStateToProps = (state) => {
        return {
            initialLoadingFinished: initialLoadingFinishedSelector(state)
        }
}

const mapActionsToProps = {
    executeInitialLoad
}

export const InitialLoading = connect(mapStateToProps, mapActionsToProps)(InitialLoadingClass)


