/**
 * Created by Sebastian Venhuis on 21.02.2021.
 */
import * as React from "react";
import {connect} from "react-redux";
import {CalcPanelVisible, closeAllMathPanel, ConverterPanelVisible} from "../../state/MathState";
import {Popup} from "../Utils/Popup/Popup";
import {UnitConverter} from "./UnitConverter";

class MathControllerClass extends React.Component{
    render(){
        console.log(this.props.calcVisible, this.props.converterPanelVisible);
        return <div>
            {this.props.calcVisible ? <Popup title={"Calculator"} onClose={()=>{this.props.closeAllMathPanel()}}></Popup> : null}
            {this.props.converterPanelVisible ? <Popup title={"Unit Converter"} onClose={()=>{this.props.closeAllMathPanel()}}>
                <UnitConverter/>
            </Popup> : null}
        </div>
    }
}

const mapStateToProps = (state)=>{
    return {
        calcVisible: CalcPanelVisible(state),
        converterPanelVisible: ConverterPanelVisible(state)
    }
}

const mapActionsToProps = {
    closeAllMathPanel
}

export const MathController = connect(mapStateToProps, mapActionsToProps)(MathControllerClass)
