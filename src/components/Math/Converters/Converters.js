/**
 * Created by Sebastian Venhuis on 24.02.2021.
 */
import {FeetToMetersConverter} from "./FeetToMeters";

export const Converters = [
    new FeetToMetersConverter()
]
