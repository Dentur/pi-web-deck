/**
 * Created by Sebastian Venhuis on 23.02.2021.
 */

function CreateConverterTag(internal, display){
    return {
        InternalName: internal,
        DisplayName: display
    }
}

export const ConverterTag = {
    LENGTH: CreateConverterTag("LENGTH", "Length"),
    WEIGHT: CreateConverterTag("HEIGHT", "Weight")
}
