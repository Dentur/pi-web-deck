/**
 * Created by Sebastian Venhuis on 24.02.2021.
 */
import {BaseConverter} from "./BaseConverter";
import {ConverterTag} from "./ConverterTag";

export class FeetToMetersConverter extends BaseConverter{
    constructor(){
        super([ConverterTag.LENGTH], "Feet", "Meters", {numbersOnly: true});
    }

    calculate(input, to) {
        return {
            unit1: input * 3.2808,
            unit2: input / 3.2808
        }
    }
}
