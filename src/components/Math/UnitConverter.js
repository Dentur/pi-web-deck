/**
 * Created by Sebastian Venhuis on 23.02.2021.
 */
import * as React from "react";
import {ConverterTag} from "./Converters/ConverterTag";
import {Converters} from "./Converters/Converters";

import "./UnitConverter.css"

export class UnitConverter extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            selectedConverter: Converters[0],
            tags: [ConverterTag.LENGTH, ConverterTag.WEIGHT].map(tag=>{
                return {
                    active: true,
                    tag: tag
                }
            }),
            currentInputValue: 0
        }
    }

    render(){
        return <div className={"UnitConverter"}>
            <div className={"filter"}>
                {this.state.tags.map((tag)=>{
                    return <div
                        className={"filterElement " + (tag.active?"active":"disabled") }
                        key={tag.tag.InternalName}
                        onClick={()=>{
                            this.setState((oldState=>{
                                return{
                                    tags: oldState.tags.map(oldTag=>{
                                        if(oldTag.tag.InternalName === tag.tag.InternalName)
                                            return {
                                                ...tag,
                                                active: !tag.active
                                            }
                                        else{
                                            return oldTag;
                                        }
                                    })
                                }
                            }))
                        }}
                    >
                        {tag.tag.DisplayName}
                    </div>
                })}
                <div className={"Converters"}>
                    {Converters.map((converter)=>{
                        return <div key={converter.id}
                                    className={"ConverterSelector "+(this.state.selectedConverter === converter ? "active" : "")}
                                    onClick={()=>{this.setState({selectedConverter: converter})}}
                        >
                            {converter.renderName()}
                        </div>
                    })}
                </div>
            </div>
            <div className={"content"}>
                <div className={"Input"}>
                    {this.state.selectedConverter ? this.state.selectedConverter.renderInput((value)=>{
                        this.setState({currentInputValue: Number.parseFloat(value)})
                    }) : null}
                </div>
                <div className={"Output"}>
                    {this.state.selectedConverter ? this.state.selectedConverter.renderOutput(this.state.currentInputValue) : null}
                </div>
            </div>
        </div>
    }
}
