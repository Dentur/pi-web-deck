/**
 * Created by Sebastian Venhuis on 10.11.2020.
 */

import React from "react";
import {connect} from "react-redux";
import {
    currentPanelButtonSelector,
    currentPanelButtonsSelector,
    panelsSelector,
    updateButtonAsync
} from "../../state/PanelState";
import {currentlyEditedButtonIdSelector, currentPanelIdSelector} from "../../state/ViewState";
import PropTypes from "prop-types";
import {ButtonTypes} from "../Panel/ButtonTypes";
import {ButtonGroup, ButtonGroupButton} from "../Utils/ButtonGroup/ButtonGroup";
import "./ButtonProperties.css"
import {ButtonImage} from "../Utils/ButtonImage/ButtonImage";
import {Popup} from "../Utils/Popup/Popup";
import {ImageSelector} from "../ImageSelector/ImageSelector";
import {ActionTypes} from "../Panel/ActionTypes";
import {StatusTypes} from "../Panel/StatusTypes";
import {StatusDisplayTypes} from "../Panel/StatusDisplayTypes";
import {MqttActionTypes} from "../Panel/MqttActionTypes";
import {MathTypes} from "../Panel/MathTypes";


class ButtonPropertiesClass extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            editImage: false,
            editActionPath: false,
            editActionOptions: false,
            editMqttSendMessage: false

        }
    }

    supportedSizes = [{
        width: 2,
        height: 2,
        svg: "Grid_Size/1_1.svg"
    },{
        width: 2,
        height: 4,
        svg: "Grid_Size/2_1.svg"
    },{
        width: 4,
        height: 2,
        svg: "Grid_Size/1_2.svg"
    },{
        width: 4,
        height: 4,
        svg: "Grid_Size/2_2.svg"
    },{
        width: 6,
        height: 6,
        svg: "Grid_Size/3_3.svg"
    },]

    getButtonSizeType(width, height){
        return `BUTTON_SIZE_${width}_${height}`
    }

    getButtonSize(buttonSizeType){
        return this.supportedSizes.find((size)=>{
            return buttonSizeType === this.getButtonSizeType(size.width, size.height);
        })
    }

    getActionTypeDefaultValue(actionType){
        switch(actionType){
            case ButtonTypes.LINK:
                return {
                    linkOptions: {
                        to: ""
                    }
                }
            case ButtonTypes.ACTION:
                return {
                    actionOptions:{
                        type: ActionTypes.START_PROGRAM,
                        path: "",
                        options: ""
                    }
                }

            case ButtonTypes.STATUS:
                return {
                    statusOptions:{
                        sensor: StatusTypes.CPU,
                        displayType: StatusDisplayTypes.SINGLE,
                        average: false,
                        options: ""
                    }
                }
            case ButtonTypes.MQTT:
                return {
                    mqttOptions:{
                        type: MqttActionTypes.SEND,
                        sendOptions: {
                            topic: "",
                            message: ""
                        }
                    }
                }
            case ButtonTypes.MATH:
                return {
                    mathOptions:{
                        type: MathTypes.CALC
                    }
                }
            default:
                return {}
        }
    }


    render(){
        if(!this.props.button)
            return <div className={"ButtonProperties"}/>


        let button = this.props.button;
        return <div className={"ButtonProperties"}>
            <div className={"Field"}>
                <span>Size</span>
                <ButtonGroup value={this.getButtonSizeType(button.layoutOptions.width, button.layoutOptions.height)}
                             onSelect={(value) => {
                                let selectedSize = this.getButtonSize((value));
                                console.log("Clicked Size", selectedSize)
                                this.props.updateButtonAsync(this.props.panelId, this.props.buttonId, {
                                    ...this.props.button,
                                    layoutOptions: {
                                        ...this.props.button.layoutOptions,
                                        width: selectedSize.width,
                                        height: selectedSize.height
                                    }
                                });
                            }}
                >
                    {this.supportedSizes.map((size)=><ButtonGroupButton key={size.svg} value={this.getButtonSizeType(size.width, size.height)}>
                        <img className={"svgImg"} src={size.svg} style={{fill: "white"}}/>
                    </ButtonGroupButton>)}
                </ButtonGroup>
            </div>
            <div className={"Field"}>
                <span>Typ</span>
                <ButtonGroup value={button.type} onSelect={(value) => {
                    this.props.updateButtonAsync(this.props.panelId, this.props.buttonId, {
                        ...this.props.button,
                        type: value,
                        ...this.getActionTypeDefaultValue(value)
                    });
                }}>
                    <ButtonGroupButton value={ButtonTypes.LINK}>Link</ButtonGroupButton>
                    <ButtonGroupButton value={ButtonTypes.STATUS}>Status</ButtonGroupButton>
                    <ButtonGroupButton value={ButtonTypes.ACTION}>Action</ButtonGroupButton>
                    <ButtonGroupButton value={ButtonTypes.MQTT}>MQTT</ButtonGroupButton>
                    <ButtonGroupButton value={ButtonTypes.MATH}>MQTT</ButtonGroupButton>
                    <ButtonGroupButton value={ButtonTypes.RELOAD}>Reload Panels</ButtonGroupButton>
                </ButtonGroup>
            </div>
            <div className={"Field"}>
                <span>Name</span>
                <input type={"text"} value={this.props.button.name}  onChange={(event)=>{
                    this.props.updateButtonAsync(this.props.panelId, this.props.buttonId, {
                        ...this.props.button,
                        name: event.target.value
                    });
                }}/>
            </div>
            { [ButtonTypes.ACTION, ButtonTypes.LINK, ButtonTypes.MQTT, ButtonTypes.MATH].includes(this.props.button.type) ?
            <div className={"Field"}>
                <span>Image</span> <ButtonImage onClick={()=>{this.setState({editImage: true})}} style={{border: "1px solid black"}} image={this.props.button.image}/>
                {this.state.editImage ?
                    <Popup title={"Images"} onClose={()=>this.setState({editImage: false})}>
                        <ImageSelector selected={this.props.button.image}
                                       onClose={()=>this.setState({editImage: false})}
                                       onSelected={(selectedImage)=>{
                                            this.props.updateButtonAsync(this.props.panelId, this.props.buttonId, {
                                                ...this.props.button,
                                                image: selectedImage
                                            });
                                            this.setState({editImage: false})
                                        }}
                        />
                    </Popup> : null
                }
            </div>: null}
            {[ButtonTypes.ACTION].includes(this.props.button.type) ?
            <div>
                <div className={"Field"}>
                    <span>Action Type</span>
                    <ButtonGroup value={this.props.button.actionOptions.type} onSelect={(value)=>{
                        this.props.updateButtonAsync(this.props.panelId, this.props.buttonId, {
                            ...this.props.button,
                            actionOptions: {
                                ...this.props.button.actionOptions,
                                type: value
                            }
                        })
                    }}>
                        <ButtonGroupButton value={ActionTypes.START_PROGRAM}>Start Program</ButtonGroupButton>
                        <ButtonGroupButton value={ActionTypes.COMMAND}>Bash Command</ButtonGroupButton>
                        <ButtonGroupButton value={ActionTypes.OPEN_WEB_PAGE}>Open Web Page</ButtonGroupButton>
                    </ButtonGroup>
                </div>
                <div className={"Field"}>
                    <span>Command</span>
                    <div className={"scrollContent"}>
                        <pre className={"language-batch"} onClick={()=>{
                            this.setState({editActionPath: true})
                        }
                        }>{this.props.button.actionOptions.path}</pre>
                    </div>
                    {this.state.editActionPath ?
                        <Popup title={"Edit Action Path"} onClose={()=>{this.setState({editActionPath: false})}}>
                            <div style={{width: 1200, height: 100}}>
                                <input type={"text"}
                                       onChange={(e)=>{
                                            this.props.updateButtonAsync(this.props.panelId, this.props.buttonId, {
                                                ...this.props.button,
                                                actionOptions: {
                                                    ...this.props.button.actionOptions,
                                                    path: e.target.value
                                                }
                                            })
                                       }}
                                       value={this.props.button.actionOptions.path}
                                       style={{width: 1180}}/>
                            </div>
                        </Popup> : null}
                </div>
                <div className={"Field"}>
                    <span>Command</span>
                    <div className={"scrollContent"}>
                        <pre className={"language-batch"} onClick={()=>{
                            this.setState({editActionOptions: true})
                        }
                        }>{this.props.button.actionOptions.options}</pre>
                    </div>
                    {this.state.editActionOptions ?
                        <Popup title={"Edit Action Options"} onClose={()=>{this.setState({editActionOptions: false})}}>
                            <div style={{width: 1200, height: 100}}>
                                <input type={"text"}
                                       onChange={(e)=>{
                                           this.props.updateButtonAsync(this.props.panelId, this.props.buttonId, {
                                               ...this.props.button,
                                               actionOptions: {
                                                   ...this.props.button.actionOptions,
                                                   options: e.target.value
                                               }
                                           })
                                       }}
                                       value={this.props.button.actionOptions.options}
                                       style={{width: 1180}}/>
                            </div>
                        </Popup> : null}
                </div>
            </div> : null}
            {[ButtonTypes.LINK].includes(this.props.button.type) ?
            <div>
                <div className={"Field"}>
                    <span>Link to</span>
                    <select className={"spdSelect"} value={this.props.button.linkOptions.to} onChange={(e)=>{
                        this.props.updateButtonAsync(this.props.panelId, this.props.buttonId,{
                            ...this.props.button,
                            linkOptions: {
                                ...this.props.button.linkOptions,
                                to: e.target.value
                            }
                        })
                    }}>
                        {this.props.panels.map(panel=>{
                            return <option key={panel.id} value={panel.id}>{panel.name}</option>
                        })}
                    </select>
                </div>
            </div> : null}
            {[ButtonTypes.STATUS].includes(this.props.button.type) ?
                <div>
                    <div className={"Field"}>
                        <span>Show stats for</span>
                        <ButtonGroup value={this.props.button.statusOptions.sensor} onSelect={(value)=>{
                            this.props.updateButtonAsync(this.props.panelId, this.props.buttonId, {
                                ...this.props.button,
                                statusOptions: {
                                    ...this.props.button.statusOptions,
                                    sensor: value
                                }
                            })
                        }}>
                            <ButtonGroupButton value={StatusTypes.CPU}>CPU</ButtonGroupButton>
                            <ButtonGroupButton value={StatusTypes.DISKS}>Disks</ButtonGroupButton>
                            <ButtonGroupButton value={StatusTypes.MEMORY}>Memory</ButtonGroupButton>
                            <ButtonGroupButton value={StatusTypes.UPTIME}>Uptime</ButtonGroupButton>
                        </ButtonGroup>
                    </div>
                    {[StatusTypes.MEMORY, StatusTypes.CPU].includes(this.props.button.statusOptions.sensor) ?
                        <div>
                            <div className={"Field"}>
                                <span>Visualisation</span>
                                <ButtonGroup value={this.props.button.statusOptions.displayType} onSelect={(value)=>{
                                    this.props.updateButtonAsync(this.props.panelId, this.props.buttonId, {
                                        ...this.props.button,
                                        statusOptions: {
                                            ...this.props.button.statusOptions,
                                            displayType: value
                                        }
                                    })
                                }}>
                                    <ButtonGroupButton value={StatusDisplayTypes.SINGLE}>Single Value</ButtonGroupButton>
                                    <ButtonGroupButton value={StatusDisplayTypes.GAUGE}>Gauge</ButtonGroupButton>
                                    <ButtonGroupButton value={StatusDisplayTypes.LINE}>Line</ButtonGroupButton>
                                </ButtonGroup>
                            </div>
                        </div> : null}
                    {[StatusTypes.DISKS].includes(this.props.button.statusOptions.sensor) ?
                        <div>
                            <div className={"Field"}>
                                <span>Visualisation</span>
                                <ButtonGroup value={this.props.button.statusOptions.displayType} onSelect={(value)=>{
                                    this.props.updateButtonAsync(this.props.panelId, this.props.buttonId, {
                                        ...this.props.button,
                                        statusOptions: {
                                            ...this.props.button.statusOptions,
                                            displayType: value
                                        }
                                    })
                                }}>
                                    <ButtonGroupButton value={StatusDisplayTypes.SINGLE}>Single Value</ButtonGroupButton>
                                    <ButtonGroupButton value={StatusDisplayTypes.GAUGE}>Gauge</ButtonGroupButton>
                                </ButtonGroup>
                            </div>
                        </div> : null}
                    {[StatusTypes.DISKS, StatusTypes.CPU].includes(this.props.button.statusOptions.sensor) ?
                        <div>
                            <div className={"Field"}>
                                <span>Assemble</span>
                                <input type={"checkbox"} value={this.props.button.statusOptions.average} onChange={(e)=>{
                                    this.props.updateButtonAsync(this.props.panelId, this.props.buttonId, {
                                        ...this.props.button,
                                        statusOptions: {
                                            ...this.props.button.statusOptions,
                                            average: e.target.checked
                                        }
                                    })
                                }}  />
                            </div>
                        </div> : null}
                </div> : null}
            {[ButtonTypes.MQTT].includes(this.props.button.type) ?
                <div>
                    <div className={"Field"}>
                        <span>MQTT Action Type</span>
                        <ButtonGroup value={this.props.button.mqttOptions.type} onSelect={(value)=>{
                            this.props.updateButtonAsync(this.props.panelId, this.props.buttonId, {
                                ...this.props.button,
                                mqttOptions: {
                                    ...this.props.button.mqttOptions,
                                    type: value
                                }
                            })
                        }}>
                            <ButtonGroupButton value={MqttActionTypes.NONE}>None</ButtonGroupButton>
                            <ButtonGroupButton value={MqttActionTypes.SEND}>Send</ButtonGroupButton>
                        </ButtonGroup>
                    </div>
                    {[MqttActionTypes.SEND].includes(this.props.button.mqttOptions.type) ?
                        <div>
                            <div className={"Field"}>
                                <span>Topic</span>
                                <input type={"text"} value={this.props.button.mqttOptions.sendOptions.topic}  onChange={(event)=>{
                                    this.props.updateButtonAsync(this.props.panelId, this.props.buttonId, {
                                        ...this.props.button,
                                        mqttOptions: {
                                            ...this.props.button.mqttOptions,
                                            sendOptions: {
                                                ...this.props.button.mqttOptions.sendOptions,
                                                topic: event.target.value
                                            }
                                        }
                                    });
                                }}/>
                            </div>
                            <div className={"Field"}>
                                <span>Message</span>
                                <div className={"scrollContent"}>
                                    <pre className={"language-batch"} onClick={()=>{
                                        this.setState({editMqttSendMessage: true})
                                    }}>
                                        {this.props.button.mqttOptions.sendOptions.message}
                                    </pre>
                                </div>
                                {this.state.editMqttSendMessage ?
                                    <Popup title={"Edit MQTT Message"} onClose={()=>{this.setState({editMqttSendMessage: false})}}>
                                        <div style={{width: 1200, height: 100}}>
                                            <input type={"text"}
                                                   onChange={(e)=>{
                                                       this.props.updateButtonAsync(this.props.panelId, this.props.buttonId, {
                                                           ...this.props.button,
                                                           mqttOptions: {
                                                               ...this.props.button.mqttOptions,
                                                               sendOptions:{
                                                                   ...this.props.button.mqttOptions.sendOptions,
                                                                   message: e.target.value
                                                               }
                                                           }
                                                       })
                                                   }}
                                                   value={this.props.button.mqttOptions.sendOptions.message}
                                                   style={{width: 1180}}/>
                                        </div>
                                    </Popup> : null}
                            </div>
                        </div> : null
                    }
                </div> : null}
            {[ButtonTypes.MATH].includes(this.props.button.type) ?
                <div>
                    <div className={"Field"}>
                        <span>Math Type</span>
                        <ButtonGroup value={this.props.button.mathOptions.type} onSelect={(value)=>{
                            this.props.updateButtonAsync(this.props.panelId, this.props.buttonId, {
                                ...this.props.button,
                                mathOptions: {
                                    ...this.props.button.mathOptions,
                                    type: value
                                }
                            })
                        }}>
                            <ButtonGroupButton value={MathTypes.CALC}>Calculator</ButtonGroupButton>
                            <ButtonGroupButton value={MathTypes.UNIT_CONVERTER}>Unit Converter</ButtonGroupButton>
                        </ButtonGroup>
                    </div>
                </div> : null}
        </div>
    }
}

ButtonPropertiesClass.propTypes = {
    button: PropTypes.shape({
        id: PropTypes.string.isRequired,
        type: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        image: PropTypes.string,
        layoutOptions: PropTypes.shape({
            positionX: PropTypes.number.isRequired,
            positionY: PropTypes.number.isRequired,
            width: PropTypes.number.isRequired,
            height: PropTypes.number.isRequired
        }),
        linkOptions: PropTypes.shape({
            to: PropTypes.string.isRequired
        }),
        actionOptions:PropTypes.shape({
            type: PropTypes.string.isRequired,
            path: PropTypes.string.isRequired,
            options: PropTypes.string
        }),
        displayOptions: PropTypes.shape({
            backgroundColor: PropTypes.string,
            fontColor: PropTypes.string
        })
    }).isRequired,
}

const mapStateToProps = (state)=>{
    return{
        panelId: currentPanelIdSelector(state),
        buttonId: currentlyEditedButtonIdSelector(state),
        button: currentPanelButtonSelector(state, currentlyEditedButtonIdSelector(state)),
        panels: panelsSelector(state)
    }
}

const mapActionsToProps = {
    updateButtonAsync
}

export const ButtonProperties = connect(mapStateToProps, mapActionsToProps)(ButtonPropertiesClass)
