/**
 * Created by Sebastian Venhuis on 02.11.2020.
 */

import React from "react";
import PropTypes from "prop-types"
import "./PanelButton.css"
import {ButtonTypes} from "../Panel/ButtonTypes";
import {currentPanelButtonSelector} from "../../state/PanelState";
import {connect} from "react-redux";
import {editButton, editingModeEnabledSelector, showPanel} from "../../state/ViewState";
import {imageSelector} from "../../state/ImageState";
import {executeAction} from "../../state/ActionState";
import {DiagramController} from "../Diagram/DiagramController";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import {faRedo} from "@fortawesome/free-solid-svg-icons";
import {library} from "@fortawesome/fontawesome-svg-core";
import {MqttActionTypes} from "../Panel/MqttActionTypes";
import {executeMqttAction} from "../../state/MqttState";
import {MathTypes} from "../Panel/MathTypes";
import {openMathPanelAsync} from "../../state/MathState";
import {MathButton} from "../Math/MathControllerClass";

library.add(faRedo);

class PanelButtonClass extends React.Component{
    constructor(props) {
        super(props);
    }

    onClick(){
        if(this.props.editing){
            this.props.editButton(this.props.button.id)
        }
        else{
            if(this.props.button.type === ButtonTypes.LINK){
                this.props.showPanel(this.props.button.linkOptions.to)
            }
            if(this.props.button.type === ButtonTypes.ACTION){
                let actionOptions = this.props.button.actionOptions
                this.props.executeAction({
                    type: actionOptions.type,
                    command: actionOptions.path,
                    options: actionOptions.options
                })
            }
            if(this.props.button.type === ButtonTypes.MQTT){
                if(this.props.button.mqttOptions.type === MqttActionTypes.SEND){
                    this.props.executeMqttAction({type: MqttActionTypes.SEND, ...this.props.button.mqttOptions.sendOptions})
                }
                else{
                    console.log(`No action defined for ${this.props.button.mqttOptions.type}`);
                }
            }
            if(this.props.button.type === ButtonTypes.MATH){
                this.props.openMathPanelAsync(this.props.button.mathOptions.type)
            }
        }
    }

    renderButtonContent(){
        switch (this.props.button.type){
            case ButtonTypes.MATH:
            case ButtonTypes.ACTION:
            case ButtonTypes.LINK:
            case ButtonTypes.MQTT:
                if(this.props.image){
                    return <img className={"panelButtonImage"} src={this.props.image?.data}/>
                }
                else {
                    return null;
                }
            case ButtonTypes.STATUS:
                return <DiagramController options={this.props.button.statusOptions}/>
            case ButtonTypes.RELOAD:
                return <div className={"reloadButton"} onClick={()=>{
                    window.location.reload(false);
                }}><FontAwesomeIcon icon={faRedo} size={"3x"}/></div>
            default:
                return <span>Unknown button type {this.props.button.type}</span>
        }
    }

    render(){
        let style = {
            backgroundColor: "#606060",
            color: "#ffffff"
        }
        let displayOptions = this.props.button.displayOptions
        if(displayOptions){
            if(displayOptions.backgroundColor)
                style.backgroundColor = displayOptions.backgroundColor;
            if(displayOptions.fontColor)
                style.color= displayOptions.fontColor;
        }
        return <div className={"panelButton"} style={style} onClick={()=>this.onClick()}>
            <span className={"spacer"}/>
            {this.renderButtonContent()}
            <span className={"panelButtonName"}>{this.props.button.name}</span>
        </div>
    }
}

PanelButtonClass.propTypes = {
    button: PropTypes.shape({
        id: PropTypes.string.isRequired,
        type: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        image: PropTypes.string,
        layoutOptions: PropTypes.shape({
            positionX: PropTypes.number.isRequired,
            positionY: PropTypes.number.isRequired,
            width: PropTypes.number.isRequired,
            height: PropTypes.number.isRequired
        }),
        linkOptions: PropTypes.shape({
            to: PropTypes.string.isRequired
        }),
        actionOptions:PropTypes.shape({
            type: PropTypes.string.isRequired,
            path: PropTypes.string.isRequired,
            options: PropTypes.string
        }),
        displayOptions: PropTypes.shape({
            backgroundColor: PropTypes.string,
            fontColor: PropTypes.string
        })
    }).isRequired,
    buttonId: PropTypes.string.isRequired
}

const mapStateToProps = (state, props)=>{
    let button = currentPanelButtonSelector(state, props.buttonId)
    return {
        button: button,
        editing: editingModeEnabledSelector(state),
        image: imageSelector(state, button.image)
    }
}

const mapActionsToProps = {
    editButton,
    showPanel,
    executeAction,
    executeMqttAction,
    openMathPanelAsync
}

export const PanelButton = connect(mapStateToProps, mapActionsToProps)(PanelButtonClass)
