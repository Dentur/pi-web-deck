/**
 * Created by Sebastian Venhuis on 05.01.2021.
 */

import React from "react";
import "./Diagrams.css"
import PropTypes from "prop-types"
import {GaugeDiagram} from "./GaugeDiagram";
import ResizeObserver from "react-resize-observer";

export class GaugeDiagramController extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            controllerSize: 0
        }
    }

    renderGauges(){
        let splitData = [];
        let width = Math.ceil(Math.sqrt(this.props.data.length))
        let currentIndex = 0;
        while(currentIndex < this.props.data.length){
            let subData = [];
            for(let xIndex = 0; xIndex < width; xIndex++){
                subData.push(this.props.data[currentIndex]);
                currentIndex++;
                if(currentIndex>=this.props.data.length)
                    break;
            }
            splitData.push(subData);
        }

        return splitData.map((subData, index)=>{
            return <div className={"subData"} key={"SubData"+index}>
                {
                    subData.map((entry)=><div style={{width: this.state.gaugeSize, height: this.state.gaugeSize}} key={"entry"+entry.id} className={"GaugeContainer"}>
                        <GaugeDiagram value={entry.data} size={this.state.gaugeSize} />
                    </div>)
                }
            </div>
        })

    }


    render(){
        return <div className={"GaugeDiagramController"}>
            <ResizeObserver onResize={(rect)=>{
                let controllerSize = Math.min(rect.height, rect.width);
                console.log("Resize", rect, "New controllerSize", controllerSize, "gaugeSize", controllerSize / Math.ceil(Math.sqrt(this.props.data.length)));
                if(this.state.controllerSize === controllerSize) return;
                this.setState({
                    controllerSize,
                    gaugeSize: controllerSize / Math.ceil(Math.sqrt(this.props.data.length))
                })
            }} />
            {this.renderGauges()}
        </div>
        /*
            {
                this.props.data.map(entry=><div key={entry.id} className={"GaugeContainer"}>
                    <GaugeDiagram value={entry.data} size={this.state.controllerSize / Math.ceil(Math.floor(this.props.data.length))} />
                </div>)
            }
        */
    }
}

GaugeDiagramController.propTypes = {
    data: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string.isRequired,
        data: PropTypes.number.isRequired
    })).isRequired
}
