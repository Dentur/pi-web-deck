/**
 * Created by Sebastian Venhuis on 05.01.2021.
 */

import * as React from "react";
import "./Diagrams.css"
import clone from "clone-deep"
import PropTypes from "prop-types"
import LiquidFillGauge from 'react-liquid-gauge';
import ResizeObserver from "react-resize-observer"


export class GaugeDiagram extends React.Component{

    constructor(props) {
        super(props);
    }

    render(){
        return <div style={{width:this.props.size, height:this.props.size}} className={"diagram gauge"}>
            <LiquidFillGauge
                value={this.props.value}
                width={this.props.size*0.8}
                height={this.props.size*0.8}
            />
       </div>
        /*return <div style={{width:this.props.size, height:this.props.size}} className={"diagram gauge"}>
            <div style={{backgroundColor: "red", width:this.props.size, height:this.props.size}}/>

        </div>*/
    }
}

GaugeDiagram.propTypes = {
    value: PropTypes.number
}

