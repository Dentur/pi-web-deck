/**
 * Created by Sebastian Venhuis on 03.01.2021.
 */
import * as React from "react";
import { ResponsiveLine } from '@nivo/line'
import "./Diagrams.css"
import clone from "clone-deep"


export class LineDiagram extends React.Component{
    //{ enable: false, format: '%H:%M:%S', tickRotation: 0 }
    //'%H:%M'
    render(){
        return <div className={"diagram"}>
            <ResponsiveLine
                axisBottom={{
                    enable: false,
                    format: (data)=>{
                        return `${new Date().getMinutes() - data.getMinutes()}:${new Date().getSeconds() - data.getSeconds()}`
                    },
                    tickValues: 'every 30 seconds'
                }}
                axisLeft={{
                    tickValues: [25,50,75,100]
                }}
                data={clone(this.props.data)}
                margin={{ top: 20, bottom: 20, left: 40, right: 20 }}
                xScale={{ type: 'time' }}
                yScale={{type: "linear", max: 100, min:0}}
                curve={"monotoneX"}
                lineWidth={1}
                enablePoints={false}
                enableGridX={false}
                gridYValues={[0, 25, 50, 75, 100]}
                interactive={false}
                animate={false}
            />
        </div>
    }
}
