/**
 * Created by Sebastian Venhuis on 03.01.2021.
 */
import * as React from "react";
import {connect} from "react-redux";
import {StatusDisplayTypes} from "../Panel/StatusDisplayTypes";
import {LineDiagram} from "./LineDiagram";
import {statsInTheLastMillisSelector} from "../../state/StatsState";
import "./DiagramController.css"
import {StatusTypes} from "../Panel/StatusTypes";
import {GaugeDiagramController} from "./GaugeDiagramController";


class DiagramControllerClass extends React.Component{

    generateCpuTimedData(statusOptions){
        let data = []
        if(statusOptions.average){
            data.push({
                id: "CPU Average",
                data: this.props.stats.map((stat)=>{
                    let average = 0;
                    stat.cpus.forEach((cpu)=>{
                        average += cpu.usage;
                    })
                    average = average / stat.cpus.length;
                    return {
                        x: new Date(stat.time),
                        //x: Number.parseInt(stat.time),
                        y: average
                    }
                })
            })
        }
        else{
            this.props.stats[0].cpus.forEach((cpu, index)=> {
                if (!data[index])
                    data[index] = {
                        id: `CPU ${index + 1}`,
                        data: []
                    }
            })
            this.props.stats.forEach((stat)=>{
                stat.cpus.forEach((cpu, index)=>{
                    data[index].data.push({
                        x: new Date(stat.time),
                        y: cpu.usage,
                    })
                })
            })
        }

        //console.log("generateCpuTimedData", data);
        return data;
    }

    generateCpuSingleData(statusOptions){
        let data = []
        let lastXStats = this.props.stats.filter((stat)=>{
            return (new Date() - new Date(stat.time)) < 1000*30
        });
        if(statusOptions.average){
            let overallAverage = 0;
            lastXStats.forEach((stat)=>{
                let cpuAverage = 0;
                stat.cpus.forEach((cpu)=>{
                    cpuAverage += cpu.usage;
                })
                cpuAverage = cpuAverage / stat.cpus.length;
                overallAverage += cpuAverage;
            })
            overallAverage = overallAverage / lastXStats.length;
            data.push({
                id: "CPU Average",
                data: overallAverage
            });
        }
        else{
            if(!lastXStats[0])
                return [];
            lastXStats[0].cpus.forEach((cpu, index)=> {
                if (!data[index])
                    data[index] = {
                        id: `CPU ${index + 1}`,
                        data: 0
                    }
            })

            lastXStats.forEach((stat)=>{
                stat.cpus.forEach((cpu, index)=>{
                    data[index].data += cpu.usage;
                })
            })

            data.forEach((entry)=>{
                entry.data = entry.data / lastXStats.length
            })
        }

        //console.log("generateCpuTimedData", data);
        return data;
    }

    generateRamTimedData(statusOptions){
        let data = []
        data.push({
            id: "Ram",
            data: this.props.stats.map((stat)=>{
                return {
                    x: new Date(stat.time),
                    //x: Number.parseInt(stat.time),
                    y: 100*(stat.memory.used / stat.memory.total)
                }
            })
        })


        //console.log("generateCpuTimedData", data);
        return data;
    }

    generateRamSingleData(statusOptions){
        let data = []
        let lastXStats = this.props.stats.filter((stat)=>{
            return (new Date() - new Date(stat.time)) < 1000*30
        });
        let average = 0;
        lastXStats.forEach(stat=>{
            average+= 100*(stat.memory.used / stat.memory.total);
        })
        average = average / lastXStats.length;
        data.push({
            id: "Ram",
            data:average
        })


        //console.log("generateCpuTimedData", data);
        return data;
    }

    getLineDiagramData(statusOptions){
        switch(statusOptions.sensor){
            case StatusTypes.CPU:
                return this.generateCpuTimedData(statusOptions);
            case StatusTypes.MEMORY:
                return this.generateRamTimedData(statusOptions)
            default: return [];
        }
    }

    getGaugeDiagramData(statusOptions){
        switch (statusOptions.sensor){
            case StatusTypes.CPU:
                return this.generateCpuSingleData(statusOptions);
            case StatusTypes.MEMORY:
                return this.generateRamSingleData(statusOptions);
            default: return[];
        }
    }

    renderDiagram(statusOptions){
        switch(statusOptions.displayType){
            case StatusDisplayTypes.LINE:
                return <LineDiagram data={this.getLineDiagramData(statusOptions)}/>
            case StatusDisplayTypes.GAUGE:
                return <GaugeDiagramController data={this.getGaugeDiagramData(statusOptions)}/>
            default:
                return <span>Diagram Type not supported</span>
        }
    }

    render(){
        return <div className={"diagramController"}>
            {this.props.stats && this.props.stats.length > 2 ?
                this.renderDiagram(this.props.options) :
                <span>Data is not available</span>
            }
        </div>
    }
}

const mapStateToProps = (state)=>{
    return {
        stats: statsInTheLastMillisSelector(state, 1000 * 60 * 1)
    }
}

const mapActionsToProps = {

}

export const DiagramController = connect(mapStateToProps, mapActionsToProps)(DiagramControllerClass);
