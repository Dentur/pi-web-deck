/**
 * Created by Sebastian Venhuis on 01.01.2021.
 */

import {createSlice} from "@reduxjs/toolkit";
import {Config} from "../config";
import sleep from "sleep-promise";

export const statsSlice = createSlice({
    name: "stats",
    initialState: {
        stats: [],
        maxStatsTime: 1000*60*60, //Keep 1h of Stats
        pollStats: false
    },
    reducers: {
        addStat(state, action){
            state.stats.push(action.payload);
        },
        /**
         * set the floag, if stats are polled (for internal use, use async instead)
         * @param state
         * @param action
         */
        setPollStats(state, action){
            state.pollStats = action.payload
        },
        removeOldStats(state, action){
            state.stats = state.stats.filter((stat)=>{return (new Date() - stat.time) < state.maxStatsTime})
        }
    }
});

export const {addStat, removeOldStats, setPollStats} = statsSlice.actions;
export const statsReducer = statsSlice.reducer;


export const statsStateSelector = function(state){
    return state.stats;
}

export const statsSelector = function(state){
    return statsStateSelector(state)?.stats;
}

export const statsInTheLastMillisSelector = function(state, millis){
    return statsSelector(state)?.filter((stat)=>{
        return (new Date() - new Date(stat.time)) < millis
    })
}

export const maxStatsTimeSelector = function(state){
    return statsStateSelector(state)?.maxStatsTime;
}

export const statsPollRunningSelector = function(state){
    return statsStateSelector(state)?.pollStats;
}

export const startStatsPollAsync = function(){
    return async (dispatch, getState) => {
        if(!statsPollRunningSelector(getState())){
            dispatch(setPollStats(true));
        }
        dispatch(removeOldStats())
        let result = await fetch(Config.BackendUrlBase+"/stats");
        if(result.status !== 200){
            console.warn("Could not get stats!");
        }
        else{
            let resultData = await result.json();
            dispatch(addStat({
                time: resultData.time,
                uptime: resultData.uptime,
                disks: resultData.disks,
                memory: resultData.memory,
                cpus: resultData.cpus,
                components: resultData.components
            }))
        }
        await sleep(1000*1);
        if(statsPollRunningSelector(getState())){
            dispatch(startStatsPollAsync());
        }
    }
}

export const stopStatsPollAsync = function(){
    return dispatch =>{
        dispatch(setPollStats(false));
    }
}

