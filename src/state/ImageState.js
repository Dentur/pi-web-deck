/**
 * Created by Sebastian Venhuis on 03.11.2020.
 */
import {createSlice} from "@reduxjs/toolkit";
import {Config} from "../config";

export const imagesSlice = createSlice({
    name: "images",
    initialState: {
        loaded: false,
        images: []
    },
    reducers: {
        addImage(state, action){
            const {id, name, data} = action.payload;
            state.images.push({id, name, data});
        },
        removeImage(state, action){
            const {id} = action.payload;
            state.images = state.images.filter((image)=>image.id !== id)
        },
        setImages(state, action) {
            const images = action.payload;
            state.images = images;
        },
        setImagesLoading(state, action){
            state.loaded = false;
        },
        setImagesLoadingFinished(state, action){
            state.loaded = true;
        }
    }
});

export const {addImage, removeImage, setImages, setImagesLoading, setImagesLoadingFinished} = imagesSlice.actions;
export const imagesReducer = imagesSlice.reducer;

export const addImageAsync = function({data, id, name}){
    return async dispatch => {
        let result = await fetch(Config.BackendUrlBase+"/images", {
            method: "POST",
            headers:{
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: name,
                data: data
            })
        })
        if(result.status !== 200){
            console.error("Could not Upload Image", result);
        }
        let server_id = result.id;
        dispatch(addImage({id: server_id, data, name}));
    }
}

export const removeImageAsync = function(id){
    return dispatch => {
        dispatch(removeImage({id}));
    }
}

export async function loadImages(){
    let result = await fetch(Config.BackendUrlBase+"/images", {
        method: "GET"
    });
    if(result.status !== 200){
        console.error("Could not get Images", result);
        return [];
    }
    return (await result.json()).images;
}

export const fetchImagesAsync = function(){
    return async (dispatch, getState) => {
        dispatch(setImagesLoading())

        let images = await loadImages();
        dispatch(setImages(images));

        //dispatch(setImages(imagesSelector(getState())));
        dispatch(setImagesLoadingFinished())
    }
}

export const imageStateSelector = (state)=>{
    return state.images;
}

export const imagesSelector = (state)=>{
    return imageStateSelector(state).images;
}

export const imagesLoadedSelector = (state)=>{
    return imageStateSelector(state).loaded;
}

export const imageSelector = (state, imageId)=>{
    return imagesSelector(state).find(image=>image.id === imageId);
}
