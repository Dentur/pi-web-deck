/**
 * Created by Sebastian Venhuis on 27.01.2021.
 */

import {createSelector, createSlice} from "@reduxjs/toolkit";
import {Config} from "../config";
import {v4} from "uuid";
import {MqttActionTypes} from "../components/Panel/MqttActionTypes";

export const mqttSlice = createSlice({
    name: "mqtt",
    initialState: {
        mqttActions: [],
        serviceEnabled: false
    },
    reducers: {
        addMqttAction(state, action){
            const {id, type, options} = action.payload;
            state.mqttActions.push({id, type, options, error: null});
        },
        removeMqttAction(state, action){
            const {id} = action.payload;
            state.mqttActions = state.mqttActions.filter((action)=>action.id !== id)
        },
        setMqttServiceEnabled(state, action){
            state.serviceEnabled = action.payload
        }
    }
});

export const {addMqttAction, removeMqttAction, setMqttServiceEnabled} = mqttSlice.actions;
export const mqttReducer = mqttSlice.reducer;


/**
 * Executes the mqtt action specified via thunk
 * @param options {{type: string, topic: string, message: string?}}
 * @returns {function(*): Promise<void>}
 */
export const executeMqttAction = function(options){
    return async dispatch => {
        if (options.type === MqttActionTypes.SEND) {
            let result = await fetch(Config.BackendUrlBase + "/mqtt", {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    topic: options.topic,
                    message: options.message
                })
            })
            if (result.status !== 200) {
                console.error("Could not execute command", result);
            }
            dispatch(addMqttAction({
                id: v4(),
                type: options.type,
                options: {
                    topic: options.topic,
                    message: options.message
                },
                error: result.status === 200 ? null : ((await result.json()).error_message || "Could not execute command")
            }));
        }
        else{
            console.error(`Error MqttAction ${options.type} not supported`)
        }
    }
}

export async function getMqttServiceStatus(){
    let result = await fetch(Config.BackendUrlBase+"/mqtt");
    return result.status;
}

export const checkIfMqttIsEnabled = function(){
    return async dispatch => {
        let serviceStatus = await getMqttServiceStatus()
        dispatch(setMqttServiceEnabled(serviceStatus===200));
    }
}

export function mqttStateSelector(state){
    return state.mqtt;
}

export const mqttEnabledSelector = createSelector(mqttStateSelector, (mqttState)=>{
    return mqttState.serviceEnabled;
})
