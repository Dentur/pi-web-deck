/**
 * Created by Sebastian Venhuis on 07.11.2020.
 */
import {createSelector, createSlice} from "@reduxjs/toolkit";

export const ViewSlice = createSlice({
    name: "view",
    initialState: {
        currentPanelId: "f41db05a-46e9-4400-a3a2-563563723469",
        currentlyEditedButtonId: null,
        editing: false
    },
    reducers: {
        showPanel(state, action){
            state.currentPanelId = action.payload;
        },
        setEditing(state, action){
            state.editing = action.payload;
        },
        editButton(state, action){
            state.currentlyEditedButtonId = action.payload;
        }
    }
})

export const {showPanel, setEditing, editButton} = ViewSlice.actions;
export const viewReducer = ViewSlice.reducer;

export const viewStateSelector = function(state){
    return state.view;
}

export const currentPanelIdSelector = createSelector(viewStateSelector, (view)=>{
    return view.currentPanelId;
})

export const editingModeEnabledSelector = createSelector(viewStateSelector, (view)=>{
    if(view)
        return view.editing
    else return false;
})

export const currentlyEditedButtonIdSelector = createSelector(viewStateSelector, (view)=>{
    if(view)
        return view.currentlyEditedButtonId;
    else return null;
})
