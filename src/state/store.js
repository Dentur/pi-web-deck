/**
 * Created by Sebastian Venhuis on 03.11.2020.
 */

import {combineReducers, configureStore, createStore, getDefaultMiddleware} from "@reduxjs/toolkit";
import {createReducer} from "@reduxjs/toolkit";
import {panelReducer} from "./PanelState";
import {viewReducer} from "./ViewState";
import {imagesReducer} from "./ImageState";
import {statsReducer} from "./StatsState";
import {mqttReducer} from "./MqttState";
import {initialLoadingReducer} from "./InitialLoadingState";
import {mathReducer} from "./MathState";

const appStoreReducer = combineReducers({
    panels: panelReducer,
    view: viewReducer,
    images: imagesReducer,
    stats: statsReducer,
    mqtt: mqttReducer,
    initialLoading: initialLoadingReducer,
    math: mathReducer
})

export const appStore = configureStore({
    reducer: appStoreReducer,
    middleware: getDefaultMiddleware({
        immutableCheck: false,
        serializableCheck: false
    })
})
