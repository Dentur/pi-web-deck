/**
 * Created by Sebastian Venhuis on 27.01.2021.
 */

import {createSelector, createSlice} from "@reduxjs/toolkit";
import {fetchPanels, setPanels, setPanelsLoading, setPanelsLoadingFinished} from "./PanelState";
import {getMqttServiceStatus, setMqttServiceEnabled} from "./MqttState";
import {loadImages, setImages, setImagesLoading, setImagesLoadingFinished} from "./ImageState";
import {startStatsPollAsync} from "./StatsState";

export const initialLoadingSlice = createSlice({
    name: "initialLoading",
    initialState: {
        loaded: false
    },
    reducers: {
        setInitiallyLoaded(state, action){
            state.loaded = action.payload;
        }
    }
});

export const {setInitiallyLoaded} = initialLoadingSlice.actions;
export const initialLoadingReducer = initialLoadingSlice.reducer;

export function executeInitialLoad(){
    return async dispatch => {
        try {

            //Load Panels
            dispatch(setPanelsLoading());
            let panelsAwait = fetchPanels();

            //Load Images
            dispatch(setImagesLoading())

            let imagesAwait = loadImages();


            //Check mqtt
            let mqttServiceStatusAwait = getMqttServiceStatus();

            //Finish panels
            let panels = await panelsAwait;
            dispatch(setPanels(panels));
            dispatch(setPanelsLoadingFinished())

            //Finish Images
            dispatch(setImages(await imagesAwait));

            //dispatch(setImages(imagesSelector(getState())));
            dispatch(setImagesLoadingFinished())

            //Finish Check Mqtt
            let mqttServiceStatus = await mqttServiceStatusAwait;
            dispatch(setMqttServiceEnabled(mqttServiceStatus === 200));

            //Start stats poll
            dispatch(startStatsPollAsync());

            //Finish
            dispatch(setInitiallyLoaded(true))
        }
        catch (err){
            console.error("Could not load initial State!", err)
        }
    }
}

export const initialLoadingStateSelector = (state)=>{
    return state.initialLoading;
}

export const initialLoadingFinishedSelector = createSelector(initialLoadingStateSelector, (initialLoadedState)=>{
    return initialLoadedState.loaded;
})
