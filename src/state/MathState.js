/**
 * Created by Sebastian Venhuis on 21.02.2021.
 */


import {createSlice} from "@reduxjs/toolkit";
import {Config} from "../config";
import {v4} from "uuid";
import {MathTypes} from "../components/Panel/MathTypes";

export const mathSlice = createSlice({
    name: "math",
    initialState: {
        calcPanelVisible: false,
        converterPanelVisible: false
    },
    reducers: {
        openMathPanel(state, action){
            const {type} = action.payload;
            switch(type){
                case MathTypes.CALC:
                    state.calcPanelVisible = true;
                    break;
                case MathTypes.UNIT_CONVERTER:
                    state.converterPanelVisible = true;
                    break;
                default:
                    console.warn("Type", type, "not Supported");
            }
        },
        closeAllMathPanel(state, action){
            state.calcPanelVisible = false;
            state.converterPanelVisible = false;
        }
    }
});

export const {openMathPanel, closeAllMathPanel} = mathSlice.actions;
export const mathReducer = mathSlice.reducer;

export const openMathPanelAsync = function(type){
    return async dispatch => {
        dispatch(closeAllMathPanel());
        dispatch(openMathPanel({type}));
    }
}

export const MathStateSelector = (state)=>{
    return state.math;
}

export const CalcPanelVisible = (state)=>{
    return MathStateSelector(state).calcPanelVisible;
}

export const ConverterPanelVisible = (state)=>{
    return MathStateSelector(state).converterPanelVisible;
}
