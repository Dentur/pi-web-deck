/**
 * Created by Sebastian Venhuis on 28.12.2020.
 */

import {createSlice} from "@reduxjs/toolkit";
import {Config} from "../config";
import {v4} from "uuid";

export const actionSlice = createSlice({
    name: "actions",
    initialState: {
        actions: []
    },
    reducers: {
        addAction(state, action){
            const {id, type, command, options} = action.payload;
            state.actions.push({id, action:{type, command, options}, finished: false, error: null});
        },
        removeAction(state, action){
            const {id} = action.payload;
            state.actions = state.actions.filter((action)=>action.id !== id)
        },
        finishedAction(state, action){
            const {id, error} = action.payload;
            let found_action = state.actions.find((action)=>action.id === id);
            if(found_action) {
                found_action.finished = true;
                if(error)
                    found_action.error = error;
            }
        }
    }
});

export const {addAction, removeAction, finishedAction} = actionSlice.actions;
export const actionReducer = actionSlice.reducer;

export const executeAction = function({type, command, options}){
    return async dispatch => {
        let result = await fetch(Config.BackendUrlBase+"/action", {
            method: "POST",
            headers:{
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                action_type: type,
                command,
                options
            })
        })
        if(result.status !== 200){
            console.error("Could not execute command", result);
        }
        dispatch(addAction({
            id: v4(),
            type,
            command,
            options,
            error: result.status === 200 ? null : (result.error_message || "Could not execute command")
        }));
    }
}
