/**
 * Created by Sebastian Venhuis on 03.11.2020.
 */

import {createSelector, createSlice} from "@reduxjs/toolkit";
import {PanelLayoutOptions} from "../components/Panel/PanelLayoutOptions";
import {ButtonTypes} from "../components/Panel/ButtonTypes";
import {ActionTypes} from "../components/Panel/ActionTypes";
import {StatusTypes} from "../components/Panel/StatusTypes";
import {currentPanelIdSelector} from "./ViewState";
import {v4 as uuid} from "uuid";
import sleep from "sleep-promise";
import {Config} from "../config";
import {act} from "@testing-library/react";

export const PanelsSlice = createSlice({
    name: "panels",
    initialState: {
        panels: [{
            id: "f41db05a-46e9-4400-a3a2-563563723469",
            buttons: [
                {
                    id: "3709c3e7-e3b3-48e7-9675-54c1f166bfb8",
                    type: ButtonTypes.ACTION,
                    name: "Default Action",
                    image: null,
                    layoutOptions: {
                        positionX: 0,
                        positionY: 0,
                        width: 2,
                        height: 2
                    },
                    actionOptions:{
                        type: ActionTypes.START_PROGRAM,
                        path: "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe",
                        options: ""
                    }
                },
                {
                    id: "b522cd95-0f2a-4c27-a7fc-2f504f85e359",
                    type: ButtonTypes.STATUS,
                    name: "CPU",
                    image: null,
                    layoutOptions: {
                        positionX: 0,
                        positionY: 2,
                        width: 4,
                        height: 4
                    },
                    statusOptions:{
                        statusTypes: StatusTypes.CPU,
                    }
                }],
            name: "Home",
            isHome: true,
            layout: PanelLayoutOptions.GRID
        }],
        loaded: false
    },
    reducers: {
        addPanel(state, action){
            const {panel} = action.payload;
            state.panels.push(panel);
        },
        removePanel(state, action){
            const {panelId} = action.payload;
            state.panels.filter((panel)=>(panel.id !== panelId) && (!panel.isHome));
        },
        updatePanel(state, action){
            const {panelId, panel} = action.payload;
            let panelIndex = state.panels.findIndex((panel)=>panelId===panel.id);
            if(panelIndex !== undefined)
                state.panels[panelIndex] = panel;
        },
        setPanels(state, action){
            console.log(action.payload);
            state.panels = action.payload;
        },
        setPanelsLoading(state, action){
            state.loaded = false;
        },
        setPanelsLoadingFinished(state, action){
            state.loaded = true;
        },
        addButton(state, action){
            const {button, panelId} = action.payload;
            let foundPanel = state.panels.find((panel)=>panelId === panel.id);
            if(foundPanel)
                foundPanel.buttons.add(button);
        },
        removeButton(state, action){
            const {buttonId, panelId} = action.payload;
            let foundPanel = state.panels.find((panel)=>panelId === panel.id);
            if(foundPanel)
                state = foundPanel.buttons.filter((button)=>button.id !== buttonId);
        },
        updateButton(state, action){
            const {buttonId, panelId, button} = action.payload;
            let foundPanel = state.panels.find((panel)=>panelId === panel.id);
            if(foundPanel) {
                let buttonIndex = foundPanel.buttons.findIndex((stateButton) => stateButton.id === buttonId);
                if (buttonIndex === undefined) return;
                foundPanel.buttons[buttonIndex] = button;
            }
        }
    }
})

export const {addButton, removeButton, updateButton, addPanel, removePanel, updatePanel, setPanels, setPanelsLoading, setPanelsLoadingFinished} = PanelsSlice.actions;
export const panelReducer = PanelsSlice.reducer;

export const panelsStateSelector = function(state){
    return state.panels;
}

export const panelsSelector = function(state){
    return panelsStateSelector(state).panels;
}

export const panelSelector = function(state, panelId){
    return panelsSelector(state).find((panel)=>panel.id === panelId)
}

export const currentPanelSelector = createSelector(currentPanelIdSelector, panelsSelector, (currentPanelId, panels)=>{
    return panels.find((panel)=>panel.id === currentPanelId);
})

export const currentPanelButtonsSelector = createSelector(currentPanelSelector, (currentPanel)=>{
    return currentPanel.buttons;
})

export const currentPanelButtonSelector = function(state, buttonId){
    return currentPanelButtonsSelector(state).find((button)=>button.id === buttonId);
}

export const panelsLoadedSelector = createSelector(panelsStateSelector, (panelsState)=>{
    return panelsState.loaded;
})

export const addPanelAsync = (panel)=>{
    return async (dispatch)=>{
        panel.isHome = false;
        let result = await fetch(Config.BackendUrlBase+"/panels", {
            method: "POST",
            headers:{
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({data: JSON.stringify(panel)})
        })
        if(result.status!==200){
            console.warn("Could not add new Panel");
            return;
        }
        let resultData = await result.json();
        panel.id = resultData.id;
        dispatch(addPanel({panel}));
    }
}

export const addDefaultPanelAsync = ()=>{
    return (dispatch)=>{
        dispatch(addPanelAsync({
            buttons: [],
            name: "Not Named Yet",
            isHome: false,
            layout: PanelLayoutOptions.GRID
        }))
    }
}

export const removePanelAsync = (panelId)=>{
    return async (dispatch, getState)=>{
        let panel = panelSelector(getState(), panelId);
        if(panel && !panel.isHome) {
            let result = await fetch(Config.BackendUrlBase+`/panel/${panelId}`, {
                method: "DELETE"
            })
            if(result.status !== 200){
                console.warn("Could not delete panel", panelId);
                return;
            }
            dispatch(removePanel({panelId}))
        }
    }
}

export const updatePanelAsync = (panelId, panel)=>{
    return async dispatch => {
        let data = JSON.stringify({
            data: JSON.stringify(panel)
        });
        let result = await fetch(Config.BackendUrlBase+`/panel/${panel.id}`,
            {
                headers:{
                    'Content-Type': 'application/json'
                },
                method: "PUT",
                body: data
            });
        if(result.status !== 200){
            console.warn("Could not update Panel");
        }
        dispatch(updatePanel({panelId, panel}));
    }
}

export async function fetchPanels(){
    let result = await fetch(Config.BackendUrlBase+"/panels");
    if(result.status!==200)
    {
        console.error("Could not get panels");
        return [];
    }
    let data = await result.json();
    return data.panels.map(panel=>{
        return {
            id: panel.id,
            ...JSON.parse(panel.data)
        }
    })
}

export const fetchPanelsAsync = ()=>{
    return async (dispatch, getState) => {
        dispatch(setPanelsLoading());
        let panels = await fetchPanels();
        dispatch(setPanels(panels));
        dispatch(setPanelsLoadingFinished())
    }
}

export const addButtonAsync = (button, panelId)=>{
    return (dispatch, getState) => {
        let currentState = getState();
        let panel = {...panelSelector(currentState, panelId)};
        panel.buttons = panel.buttons.slice();
        panel.buttons.push(button);
        dispatch(updatePanelAsync(panelId, panel));
    }
}

export const addDefaultButtonAsync=(panelId)=>{
    return (dispatch, getState)=> {
        dispatch(addButtonAsync({
            id: uuid(),
            type: ButtonTypes.ACTION,
            name: "",
            image: null,
            layoutOptions: {
                positionX: 0,
                positionY: 0,
                width: 2,
                height: 2
            },
            actionOptions:{
                type: ActionTypes.START_PROGRAM,
                path: "",
                options: ""
            }
        }, panelId));
    }
}

export const removeButtonAsync = (panelId, buttonId) => {
    return (dispatch, getState) =>{
        let currentState = getState();
        let panel = {...panelSelector(currentState, panelId)};
        panel.buttons = panel.buttons.filter((button)=>button.id !== buttonId);
        dispatch(updatePanelAsync(panelId, panel));
    }
}

export const updateButtonAsync = (panelId, buttonId, button) =>{
    return (dispatch, getState) => {
        let currentState = getState();
        let panel = {...panelSelector(currentState, panelId)};
        let buttonIndex = panel.buttons.findIndex((button)=>button.id === buttonId);
        panel.buttons = panel.buttons.slice();
        panel.buttons[buttonIndex] = button;
        dispatch(updatePanelAsync(panelId, panel));
    }
}

