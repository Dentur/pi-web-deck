import React, {useState} from "react";
import logo from './logo.svg';
import './App.css';
import {PanelController} from "./components/Panel/PanelController";
import {appStore} from "./state/store";
import {Provider} from "react-redux";
import {ButtonSidebar} from "./components/Sidebar/ButtonSidebar";
import {PanelSidebar} from "./components/Sidebar/PanelSidebar";
import IdleTimer from "react-idle-timer"
import {InitialLoading} from "./components/Utils/InitialLoading/InitialLoading";
import {MathController} from "./components/Math/MathControllerClass";

function App() {
    let [inactive, setInactive] = useState(false)
  return (
    <div className="App">
        <Provider store={appStore}>
            <InitialLoading>
                <PanelController/>
                {inactive? <div className={"inactive"}/> : null}
                <IdleTimer
                    timeout={1000*60}
                    onActive={()=>{setInactive(false)}}
                    onIdle={()=>{setInactive(true)}}
                />
                <MathController/>
                <div className={"SidebarContainer"}>
                    <PanelSidebar/>
                    <ButtonSidebar/>
                </div>
            </InitialLoading>
        </Provider>
    </div>
  );
}

export default App;
